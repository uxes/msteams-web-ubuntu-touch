import QtQuick 2.12
import QtQuick.Window 2.3
import QtWebEngine 1.9
import Morph.Web 0.1
import Lomiri.Components 1.3 as UITK
import Lomiri.Content 1.3 as UCT
import Qt.labs.platform 1.0

Window {
    id: window
    width: 600
    height: 800
    // visibility: Window.Windowed
    title: "MS Teams"

    property string dataPath: String(StandardPaths.writableLocation(StandardPaths.AppConfigLocation)).replace(/^file:\/\//, "")
    property string appName: dataPath.substring(dataPath.lastIndexOf("/") + 1)
    property string pageUrl: "https://teams.microsoft.com/_#/calendarv2"
    property bool hasLoggedIn: false

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
    }

    function generateUserAgent() {
        const operatingSystems = ["Ubuntu 22.04", "Debian", "Ubuntu", "Ubuntu Touch", "UBports", "Linux"];

        const browserVersion = `${getRandomInt(256, 1023)}.0.${getRandomInt(0, 6000)}.${getRandomInt(0, 200)}`;
        const system = `Mozilla/5.0 (Wayland; Linux; ${operatingSystems[Math.floor(Math.random() * operatingSystems.length)]})`;

        if(Math.random() > 0.5) {
            return `${system} Gecko/20100101 Firefox/${browserVersion}`;
        }
        return `${system} AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/${browserVersion} Chrome/${browserVersion} Safari/537.36`;
    }

    function shareFile(contentPicker, url) {
        const currentHeaders = {};
        webview.runJavaScript("document.cookie", function(cookies) {
            currentHeaders.Cookie = cookies;
            currentHeaders["User-Agent"] = webview.profile.httpUserAgent;

            downloadManager.download(url, currentHeaders);
            downloadManager.visible = true;
            downloadManager.fileDownloaded.connect((filePath) => {
                contentPickerLoader.setSource(contentPicker, {
                    contentType: UCT.ContentType.All,
                    handler: UCT.ContentHandler.Destination,
                    selectionType: null,
                    filePath: filePath,
                    headerText: "Download"
                });
            });
        });
    }

    Item {
        id: root
        anchors {
            fill: parent
            bottomMargin: UbuntuApplication.inputMethod.visible ? UbuntuApplication.inputMethod.keyboardRectangle.height : 0
            Behavior on bottomMargin {
                NumberAnimation {
                    duration: 175
                    easing.type: Easing.OutQuad
                }
            }
        }

        /* A webview solely for the purpose of downloading. Can't use Ubuntu.DownloadManager as we need to retain cookie headers and there is no direct way to extract them all from WebEngineView */
        WebEngineView {
            id: webviewDownloads
            visible: false

            profile.httpUserAgent: webview.profile.httpUserAgent
            profile.cachePath: webview.profile.dataPath
            // profile.downloadPath: String(StandardPaths.writableLocation(StandardPaths.CacheLocation)).replace(/^file:\/\//, "")
            profile.downloadPath: `/home/phablet/.local/share/ubuntu-download-manager/${appName}/Downloads/`
            profile.persistentStoragePath: webview.profile.dataPath

            profile.onDownloadRequested: (downloadItem) => {
                console.log("onDownloadRequested", downloadItem.url);
                downloadItem.accept();
            }

            profile.onDownloadFinished: (downloadItem) => {
                console.log(`Download finished: ${downloadItem.downloadDirectory}/${downloadItem.downloadFileName}`);

                const contentPicker = Qt.resolvedUrl("ContentHub.qml");
                contentPickerLoader.setSource(contentPicker, {
                    contentType: UCT.ContentType.All,
                    handler: UCT.ContentHandler.Destination,
                    selectionType: null,
                    filePath: `${downloadItem.downloadDirectory}/${downloadItem.downloadFileName}`,
                    headerText: "Download"
                });
            }
        }

        WebEngineView {
            id: webview
            anchors.fill: parent

            url: pageUrl
            settings.touchIconsEnabled: true
            audioMuted: false
            zoomFactor: 1.5


            profile.httpUserAgent: {
                const httpUserAgent = window.generateUserAgent();
                console.log("httpUserAgent", httpUserAgent);
                return httpUserAgent;
            }
            profile.cachePath: dataPath
            profile.persistentStoragePath: dataPath

            property string lastValidUrl: ""

            onUrlChanged: {
                let _url = String(url);
                const appUrl = /https?:\/\/teams.microsoft.com/;

                console.log(`url is: ${_url}`);

                if(!hasLoggedIn && appUrl.test(_url)) {
                    hasLoggedIn = true;
                }

                lastValidUrl = url;
            }

            onNewViewRequested: (request) => {
                console.log(`External URL requested, opening: ${request.requestedUrl}`);
                console.log(`userInitiated ${request.userInitiated ? "true" : "false"}`);
                Qt.openUrlExternally(request.requestedUrl);
            }

            onFileDialogRequested: (request) => {
                const contentPicker = Qt.resolvedUrl("ContentHub.qml");

                switch (request.mode) {
                    case FileDialogRequest.FileModeOpen:
                    case FileDialogRequest.FileModeOpenMultiple:
                        request.accepted = true;
                        contentPickerLoader.setSource(contentPicker, {
                            contentType: UCT.ContentType.All,
                            handler: UCT.ContentHandler.Source,
                            selectionType: FileDialogRequest.FileModeOpen ? UCT.ContentTransfer.Single : UCT.ContentTransfer.Multiple,
                            headerText: "Select file(s) to share"
                        });
                        if(contentPickerLoader.item) {
                            contentPickerLoader.item.imported.connect((items) => {
                                if(items) {
                                    request.dialogAccept(items);
                                }
                                else {
                                    request.dialogReject();
                                }
                            });
                        }
                        break;

                    // case FilealogRequest.FileModeUploadFolder:
                    case FileDialogRequest.FileModeSave:
                        request.accepted = true;
                        const imported = shareFile(contentPicker, url);
                        if(imported) {
                            imported.connect((items) => {
                                if(items) {
                                    request.dialogAccept(items);
                                }
                                else {
                                    request.dialogReject();
                                }
                            });
                        }
                        break;
                }
            }

            profile.onPresentNotification: (notification) => {
                console.log("notification", JSON.stringify(notification));
            }

            Component.onCompleted: {
                grantFeaturePermission(url, WebEngineView.Notifications, true);
            }
        }

        Loader {
            id: contentPickerLoader
            anchors.fill: parent
            z: 10

            onItemChanged: {
                item.imported.connect((items) => {
                    contentPickerLoader.source = ""; /* Close/unload on any response */
                });
            }
        }

        DownloadManager {
            id: downloadManager
            z: 9
            visible: false
        }
    }
}
